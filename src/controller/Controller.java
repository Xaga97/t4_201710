package controller;

import model.data_structures.ILista;
import model.data_structures.ListaEncadenada;
import model.logic.ManejadorPeliculas;
import model.vo.VOAgnoPelicula;
import model.vo.VOPelicula;

public class Controller {

	
	private static ManejadorPeliculas manejador= new ManejadorPeliculas();
	public static long ordenarArreglo(int n){
		VOPelicula[] t = manejador.generarPeliculasArreglo(n);
		long m = -System.currentTimeMillis();
		manejador.ordenarPeliculas(t);
		return m+System.currentTimeMillis();
	}
	public static long ordenarLista(int n){
		ListaEncadenada t = manejador.generarPeliculasLista(n);
		long m = -System.currentTimeMillis();
		manejador.ordenarPeliculasL(t);
		return m+System.currentTimeMillis();
	}

}
