package model.data_structures;

import java.util.Iterator;


public class ListaDobleEncadenada< T extends Comparable<T> > implements ILista<T>
{
	private Casilla<T> inicial;

	private int currentSize;
	
	private Casilla<T> actual;

	public ListaDobleEncadenada() 
	{
		// TODO Auto-generated constructor stub

		inicial = null;
		currentSize = 0;
		actual=null;
	}
	public static class Casilla<T> {
		private T elemento;
		private Casilla<T> siguiente;
		private Casilla<T> anterior;

		public Casilla(T pElemento) {
			// TODO Auto-generated constructor stub
			elemento = pElemento;
			siguiente = null;
			anterior = null;
		}
		public Casilla<T> darSiguiente() {
			return siguiente;
		}
		public Casilla<T> darAnterior(){
			return anterior;
		}
		public void setSig(Casilla<T> c) {
			this.siguiente = c;
		}
		public void setAnt(Casilla<T> c){
			this.anterior = c;
		}
		public T darElemeto() {
			return elemento;
		}
	}
	public Iterator<T> iterator() {
		// TODO Auto-generated method stub
		return new MyIterator(inicial);
	}
	
	public class MyIterator implements Iterator<T>{

		private Casilla<T> actual;
		public MyIterator(Casilla<T> inicial) {
			// TODO Auto-generated constructor stub
			actual = inicial;	
		}
		public boolean hasNext() {
			// TODO Auto-generated method stub
			return actual.darSiguiente() != null ? true : false;
		}
		public T next() {
			// TODO Auto-generated method stub
			return (T) actual.darSiguiente();
		}
		@Override
		public void remove() {
			// TODO Auto-generated method stub
			
		}
	}

	public void agregarElementoFinal(T elem) {
		Casilla<T> casillaActual = inicial;
		if (inicial == null){
			inicial = new Casilla<T>(elem);
			actual = inicial;}
		else {
			for (; casillaActual.darSiguiente() != null;) {
				casillaActual = casillaActual.darSiguiente();
			}
			Casilla<T> nueva = new Casilla<T>(elem);
			nueva.setAnt(casillaActual);
			casillaActual.setSig(nueva);
		}
		currentSize++;
		
	}

	public T darElemento(int pos) {
		Casilla<T> casillaActual = inicial;
		for (int i = 0; i < pos; i++)
			try {
				casillaActual = casillaActual.darSiguiente();	
			} catch (Exception e) {
				// TODO: handle exception
				casillaActual=null;
				break;
			}
		return (T) casillaActual;
	}


	public int darNumeroElementos() {
		return currentSize;
	}

	public T darElementoPosicionActual() {
		// TODO Auto-generated method stub
		return (T) actual;
	}

	public boolean avanzarSiguientePosicion() {
		if(actual.darSiguiente()!=null){
			actual=actual.darSiguiente();
			return true;
		}
		return false;
		
	}

	public boolean retrocederPosicionAnterior() {
		if(actual.darAnterior()!=null){
			actual=actual.darAnterior();
			return true;
		}
		return false;
	}

	public T buscarElemento( T elem )
	{
		boolean encontro = false;
		Casilla<T> casActual = inicial;
		T buscado = null;
		for( int i = 0; i < currentSize && !encontro; i++ )
		{
			try 
			{
				if( casActual.darElemeto().compareTo(elem) == 1 )
				{
					buscado = casActual.darElemeto();
					encontro = true;
				}
				else
				{
					casActual = casActual.darSiguiente();	
				}
			} 
			catch (Exception e) 
			{
				casActual=null;
				break;
			}
		}
		return buscado;
	}

	public void clear() {
		// TODO Auto-generated method stub
		inicial = null;
		currentSize = 0;
		actual=null;
	}
	
}
