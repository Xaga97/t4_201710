package model.data_structures;

import java.util.Iterator;

public class ListaEncadenada<T> implements ILista<T> 
{
	private Casilla inicial;
	private int currentSize;
	private Casilla actual;
	public ListaEncadenada() {
		// TODO Auto-generated constructor stub

		actual=inicial = null;
		currentSize = 0;

	}
	public static class Casilla<T> {
		private T elemento;
		private Casilla siguiente;

		public Casilla(T pElemento) {
			// TODO Auto-generated constructor stub
			elemento = pElemento;
			siguiente = null;
		}

		public Casilla darSiguiente() {
			return siguiente;
		}

		public void setSig(Casilla c) {
			this.siguiente = c;
		}

		public T darElemeto() {
			return elemento;
		}

	}

	public Iterator<T> iterator() {
		// TODO Auto-generated method stub
		return new MyIterator(inicial);
	}

	private static class MyIterator<T> implements Iterator{

		private Casilla actual;
		public MyIterator(Casilla inicial) {
			// TODO Auto-generated constructor stub
			actual = inicial;	
		}
		public boolean hasNext() {
			// TODO Auto-generated method stub
			return actual.darSiguiente() != null ? true : false;
		}
		public Object next() {
			// TODO Auto-generated method stub
			return actual.darSiguiente();
		}
		@Override
		public void remove() {
			// TODO Auto-generated method stub

		}
	}
	public Casilla getInicial() {
		return inicial;
	}

	public void setInicial(T element) {
		this.inicial = new Casilla(element);
	}
	public void agregarElementoFinal(T elem) {
		Casilla casillaActual = inicial;
		if (inicial == null){
			inicial = new Casilla<T>(elem);
			actual=inicial;
		}
		else {
			for (; casillaActual.darSiguiente() != null;) {
				casillaActual = casillaActual.darSiguiente();
			}
			casillaActual.setSig(new Casilla<T>(elem));
		}
		currentSize++;

	}

	@SuppressWarnings("unchecked")
	public T darElemento(int pos) {
		Casilla casillaActual = inicial;
		for (int i = 0; i < pos; i++)
			try {
				casillaActual = casillaActual.darSiguiente();	
			} catch (Exception e) {
				// TODO: handle exception
				casillaActual=null;
				break;
			}
		return (T) casillaActual.elemento;
	}


	public int darNumeroElementos() {
		return currentSize;
	}

	public T darElementoPosicionActual() {
		return (T) actual;
	}

	public boolean avanzarSiguientePosicion() {
		if(actual.darSiguiente()!=null){
			actual=actual.darSiguiente();
			return true;
		}
		return false;
	}

	public boolean retrocederPosicionAnterior() {
		if(actual!=inicial){
			Casilla n = inicial;
			while(n.darSiguiente()!=actual){
				n=n.darSiguiente();
			}
			actual=n;
			return true;
		}
		return false;
	}

}
