package model.logic;

import model.data_structures.ILista;
import model.data_structures.ListaDobleEncadenada;
import model.data_structures.ListaEncadenada;
import model.vo.VOAgnoPelicula;
import model.vo.VOPelicula;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Random;


public class ManejadorPeliculas  {

	private ListaEncadenada<VOPelicula> misPeliculas;
	
	private ListaDobleEncadenada<VOAgnoPelicula> peliculasAgno;
	public VOPelicula[] ordenarPeliculas(VOPelicula[] s){
		VOPelicula[]m= s;
		VOPelicula auxiliar[] = s;
		for(int v = 2; v/2<m.length;v*=2){
			for(int posi=0;posi<m.length && posi+v/2 < m.length;posi+=v){
				int posm=posi+v/2;
				int posf=Math.min(posi+v,m.length);
				for(int i = posi, d = posm, pi=posi;i<posm||d<posf;){
					if(i==posm){
						auxiliar[pi++]=m[d++];
					}
					else if(d==posf){
						auxiliar[pi++]= m[i++];
					}else{
						if(m[i].compareTo(m[d])<0){
							auxiliar[pi++]=m[i++];
						}
						else{
							auxiliar[pi++]=m[d++];
						}
					}
				}
				for(int o = posi;o<posf;o++){
					m[o]=auxiliar[o];
				}	    	
			}
		}
		for(int f=0;f<m.length;f++){
			m[f]=auxiliar[f];
		}
		return m;
	}
	public ListaEncadenada<VOPelicula> ordenarPeliculasL(ListaEncadenada s) {
		ListaEncadenada<VOPelicula> a = new ListaEncadenada<VOPelicula>();
		VOPelicula[]m= new VOPelicula[s.darNumeroElementos()];
		for(int i=0;i<s.darNumeroElementos();i++)m[i]=(VOPelicula) s.darElemento(i);
		VOPelicula auxiliar[] = new VOPelicula[s.darNumeroElementos()];
		for(int v = 2; v/2<m.length;v*=2){
			for(int posi=0;posi<m.length && posi+v/2 < m.length;posi+=v){
				int posm=posi+v/2;
				int posf=Math.min(posi+v,m.length);
				for(int i = posi, d = posm, pi=posi;i<posm||d<posf;){
					if(i==posm){
						auxiliar[pi++]=m[d++];
					}
					else if(d==posf){
						auxiliar[pi++]= m[i++];
					}else{
						if(m[i].compareTo(m[d])<0){
							auxiliar[pi++]=m[i++];
						}
						else{
							auxiliar[pi++]=m[d++];
						}
					}
				}
				for(int o = posi;o<posf;o++){
					m[o]=auxiliar[o];
				}	    	
			}
		}
		for(int f=0;f<m.length;f++){
			a.agregarElementoFinal(m[f]);
		}
		return a;
		
	}
	
	public ILista<VOPelicula> generarPeliculasAleatorio( int number )
	{
		ILista<VOPelicula> lista = new ListaEncadenada<VOPelicula>();
		for( int i = 0; i < number; i++ )
		{
			Random rnd = new Random();
			int posicion = rnd.nextInt(misPeliculas.darNumeroElementos());
			lista.agregarElementoFinal( misPeliculas.darElemento(posicion) );
		}
		
		return lista;
		
	}                        
	
	public VOPelicula[] generarPeliculasArreglo(int n)
	{
		VOPelicula[] array =new VOPelicula[n];
		for( int i = 0; i < n; i++ )
		{
			Random rnd = new Random();
			int posicion = rnd.nextInt(misPeliculas.darNumeroElementos());
			array[i]= misPeliculas.darElemento(posicion);
		}
			
		return array;
			
	}

}