package model.vo;

import model.data_structures.ILista;

public class VOPelicula implements Comparable<VOPelicula>{
	
	private String titulo;
	private int agnoPublicacion;
	private ILista<String> generosAsociados;
	
	
	public VOPelicula( String pTitulo, int pAgno, ILista<String> pLista ) 
	{
		titulo = pTitulo;
		agnoPublicacion = pAgno;
		generosAsociados = pLista;
		
	}
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	
	public int getAgnoPublicacion() {
		return agnoPublicacion;
	}
	public void setAgnoPublicacion(int agnoPublicacion) {
		this.agnoPublicacion = agnoPublicacion;
	}
	
	public ILista<String> getGenerosAsociados() {
		return generosAsociados;
	}
	public void setGenerosAsociados(ILista<String> generosAsociados) {
		this.generosAsociados = generosAsociados;
	}
	public int compareTo(VOPelicula arg0) {
		return titulo.compareTo(arg0.getTitulo());
	}
	

}