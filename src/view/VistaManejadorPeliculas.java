package view;

import java.util.Scanner;

import model.data_structures.ILista;
import model.vo.VOAgnoPelicula;
import model.vo.VOPelicula;
import controller.Controller;

public class VistaManejadorPeliculas {
	
	public static void main(String[] args) {
		
		
		Scanner sc=new Scanner(System.in);
		boolean fin=false;
		while(!fin){
			printMenu();
			
			int option = sc.nextInt();
			
			switch(option){
				case 1:
					System.out.println("Por favor ingrese cu�ntos datos desea generar para el arreglo");
					int n = sc.nextInt();
					System.out.println("El tiempo que tom� en ordenar es "+Controller.ordenarArreglo(n));
					break;
				case 2:
					System.out.println("Por favor ingrese cu�ntos datos desea generar para la lista");
					int m = sc.nextInt();
					System.out.println("El tiempo que tom� en ordenar es "+Controller.ordenarLista(m));
					break;
				case 3:	
					fin=true;
					break;
			}
			
			
		}
	}

	private static void printMenu() {
		System.out.println("---------ISIS 1206 - Estructuras de datos----------");
		System.out.println("---------------------Taller 4----------------------");
		System.out.println("1. Ordenar un arreglo de n datos aleatorios)");
		System.out.println("2. Ordenar una lista de n datos aleatorios");
		System.out.println("3. Salir");
		
	}

}
